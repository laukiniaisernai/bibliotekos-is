<?php

require_once(LIB_PATH . DS . "configs.php");

class MySQLDatabase {

    private $connection;
    public $last_query; //error check
    private $magic_quotes_active;
    private $real_escape_string_exists;

    function __construct() {
        $this->open_connection();
        $this->magic_quotes_active = get_magic_quotes_gpc();
        $this->real_escape_string_exists = function_exists("mysql_real_escape_string"); //exists on php v4.3.0 or higher
    }

    public function open_connection() { //opens connection to db ( uses configs.php for the variables )
        $this->connection = mysql_connect(DB_SERVER, DB_USER, DB_PASS);
        mysql_set_charset('utf8mb4', $this->connection);
        if(!$this->connection){
            die ("Database connection failed:" . mysql_error());
        } else {
            $db_select = mysql_select_db(DB_NAME, $this->connection);
            if(!$db_select){
                die("Couldn't select database:" . mysql_error());
            }
        }
    }

    public function close_connection() { //close the cnnection to the db
        if(isset($this->connection)){
            mysql_close($this->connection);
            unset($this->connection);
        }
    }

    public function query($sql) { //does my magic for the database
        $this->last_query = $sql;
        $result = mysql_query($sql, $this->connection);
        $this->confirm_query($result);
        return $result;
    }

    public function escape_value($value) { //used to test for older versions of php ( to escape strings )
        if($this->real_escape_string_exists){
            if($this->magic_quotes_active) {$value=stripslashes($value);}
            $value=mysql_real_escape_string($value);
        } else {
            if(!$this->magic_quotes_active) {$value=addslashes($value);}
        }
        return $value;
    }

    public function fetch_array($result) {
        //returns an array of elements in the table row
        return $result = mysql_fetch_array($result);
    }

    public function num_rows($result) {
        //returns amount of rows in table
        return mysql_num_rows($result);
    }

    public function insert_id() {
        //returns last id inserted to the current db
        return mysql_insert_id($this->connection);
    }

    public function affected_rows() {
        //returns rows affected by last insertion
        return mysql_affected_rows($this->connection);
    }

    private function confirm_query($result_set){
        if (!$result_set) {
            $output = "Database query failed:" . mysql_error() . "<br/><br/>";
            $output.="Last SQL query: " . $this->last_query;
            die($output); //check if last query returned errors
        }
    }
}

$database = new MySQLDatabase();

?>