<?php
/**
 * Created by PhpStorm.
 * User: Mantas
 * Date: 12/9/2015
 * Time: 17:45
 */

    require_once(LIB_PATH.DS."database.php");

    class City extends DatabaseObject {
        protected static $table_name="miestai";
        protected static $db_fields = array('miestas_id','miesto_pavadinimas');

        public $miestas_id;
        public $miesto_pavadinimas;

        public static function find_city($city_id) {
            global $database;
            $sql = "SELECT * FROM " . static::$table_name . " WHERE miestas_id=". $database->escape_value($city_id) . " LIMIT 1";
            return static::find_by_sql($sql);
        }
    }

?>