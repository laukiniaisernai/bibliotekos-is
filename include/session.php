<?php
class Session { //cia, klase, kad vartotojas butu prisijunges, ir sistema tai zinotu, nu krc :D

    private $logged_in=false;
    public $vartotojas_id;
    public $message;

    function __construct() {
        session_start();
        $this->check_message();
        $this->check_login();
    }

    public function is_logged_in() {
        return $this->logged_in;
    }

    public function return_session_id () {
        if(isset($_SESSION['vartotojas_id'])) {
            return $this->vartotojas_id;
        } else {
            return "The session id is not set.";
        }
    }

    public function login($user) {
        if($user) {
            $this->vartotojas_id = $_SESSION['vartotojas_id'] = $user->vartotojas_id;
            $this->logged_in = true;
        }
    }

    public function logout(){
        unset($_SESSION['vartotojas_id']);
        unset($this->vartotojas_id);
        $this->logged_in=false;
    }

    private function check_login() {
        if(isset($_SESSION['vartotojas_id'])) {
            $this->vartotojas_id = $_SESSION['vartotojas_id'];
            $this->logged_in = true;
        } else {
            unset($this->vartotojas_id);
            $this->logged_in = false;
        }
    }

    private function check_message() {
        if(isset($_SESSION['message'])) {
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        } else {
            $this->message="";
        }
    }

    public function message($msg="") {
        if(!empty($msg)) {//$this->message = $msg won't work as it's just a variable, I NEED TO SET IT TO THE SESSION VARIABLE
            $_SESSION['message'] = $msg;
        } else {
            return $this->message;
        }
    }
}
$session = new Session();
$message = $session->message();

?>