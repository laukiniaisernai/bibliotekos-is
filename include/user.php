<?php

require_once(LIB_PATH.DS."database.php");

class User extends DatabaseObject {

    protected static $table_name="vartotojai";
    protected static $db_fields = array('vartotojas_id','vardas','pavarde','el_pastas','tel_nr','delspinigiai','ar_uzblokuotas','miestas_id','busenos_id','slaptazodis');

    public $vartotojas_id;
    public $vardas; //perrasiau i LT nes mysql man ten knisosi
    public $pavarde;
    public $el_pastas;
    public $tel_nr;
    public $delspinigiai;
    public $ar_uzblokuotas;
    public $miestas_id;
    public $busenos_id;
    public $slaptazodis;

    public function full_name() {
        if(isset($this->vardas) && isset($this->pavarde)) {
            return $this->vardas . " " . $this->pavarde;
        } else {
            return "Something went wrong. (full_name() method)";
        }
    }

    public static function authenticate($el_pastas="", $slaptazodis="") {

        global $database; //cia globalus kintamasis, atidara mysql jungtis (zr database.php)
        $el_pastas      =$database->escape_value($el_pastas);
        $slaptazodis   =$database->escape_value($slaptazodis);

        $sql = "SELECT * FROM ".static::$table_name." WHERE el_pastas = '{$el_pastas}' AND slaptazodis = '{$slaptazodis}' LIMIT 1"; //pakeiciau pagal DB laukus
        $result_array = self::find_by_sql($sql);
        return !empty($result_array) ? array_shift($result_array) : false;
    }


    public static function find_user_by_id($user_id) {
        if(is_int($user_id)) {
            $sql="SELECT * FROM ". static::$table_name. " WHERE vartotojas_id = '{$user_id}' LIMIT 1";
            $result_array = User::find_by_sql($sql);
            return !empty($result_array) ? array_shift($result_array) : false;
        } else {

        }
    }

}

?>