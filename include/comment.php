<?php
/**
 * Created by PhpStorm.
 * User: Mantas
 * Date: 12/12/2015
 * Time: 19:46
 */

require_once(LIB_PATH.DS."database.php");

class Comment extends DatabaseObject {
    protected static $table_name = "komentarai";
    protected static $db_fields = array('komentaras_id', 'komentaras', 'knyga_id', 'vartotojas_id', 'parasymo_data');

    public $komentaras_id;
    public $komentaras;
    public $knyga_id;
    public $vartotojas_id;
    public $parasymo_data;

    public static function make($knyga_id, $vartotojas_id, $komentaras="") {
        if(!empty($knyga_id) && !empty($vartotojas_id) && !empty($komentaras)) {
            $comment = new Comment();
            $comment->knyga_id = (int)$knyga_id;
            $comment->vartotojas_id = $vartotojas_id;
            $comment->komentaras = $komentaras;
            $comment->parasymo_data = strftime("%Y-%m-%d %H:%M:%S", time());
            return $comment;
        } else {
            return false;
        }

    }

    public static function find_comments_on($knyga_id=0) {
        global $database;
        $sql = "SELECT * FROM " . static::$table_name . " WHERE knyga_id=". $database->escape_value($knyga_id) . " ORDER BY parasymo_data ASC";
        return static::find_by_sql($sql);
    }

}