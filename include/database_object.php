<?php

require_once(LIB_PATH.DS."database.php");

class DatabaseObject {

    public static function find_all() {

        return $result = static::find_by_sql("SELECT * FROM ".static::$table_name);
    }

    public static function find_by_id($id=0) {
        global $database;
        $result_array = static::find_by_sql("SELECT * FROM ".static::$table_name." WHERE ". static::$db_fields[0] ." =".$database->escape_value($id)." LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM ".static::$table_name;
        $result = $database->query($sql);
        $row = $database->fetch_array($result);
        return array_shift($row);
    }

    public static function find_by_sql($sql="") {
        global $database;
        $result = $database->query($sql);
        $object_array = array();
        while($row=$database->fetch_array($result)){
            $object_array[]=static::instantiate($row);
        }
        return $object_array;
    }
    public static function executeSql($sql, $return){
        global $database;

        if ($return == 0 )
            $database->query($sql);
        else {
            $res = $database->query($sql);
            $row =  $database->fetch_array($res);
            mysql_free_result($res);
            $id = $row[0];
        }
        return $id;

    }

    private static function instantiate($record) {
        $object = new static;

        foreach($record as $attribute=>$value) {
            if($object->has_attribute($attribute)) {
                $object->$attribute=$value;
            }
        }
        return $object;

    }

    private function has_attribute($attribute) {
        $object_vars = get_object_vars($this);
        return array_key_exists($attribute, $object_vars);
    }

    protected function attributes() {
        $attributes = array();
        foreach(static::$db_fields as $field) {
            if(property_exists($this, $field)) {
                $attributes[$field]=$this->$field;
            }
        }
        return $attributes;
    }


    protected function clean_attributes() {
        global $database;
        $clean_attributes=array();
        foreach($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public function save() {
        return !isset(static::$db_fields[0]) ? $this->update() : $this->create();
    }

    public function saveBan() {
        return isset(static::$db_fields[0]) ? $this->update() : $this->create();
    }

    protected function create() {
        global $database;
        $attributes = $this->clean_attributes();
        $sql = "INSERT INTO ".static::$table_name." (";
        $sql .=join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .="')";
        if($database->query($sql)) {
            $this->id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }


    protected function update() {
        global $database;
        $attributes = $this->clean_attributes();
        $attribute_pairs = array();
        foreach($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}='{$value}'";
        }
        $sql = "UPDATE ".static::$table_name." SET ";
        $sql .=join(", ", $attribute_pairs);
        $sql .=" WHERE ". static::$db_fields[0] . "=". $database->escape_value($this->vartotojas_id);
        $database->query($sql);
        return($database->affected_rows()== 1 ? true : false);
    }

    public function delete() {
        global $database;
        $sql = "DELETE FROM ".static::$table_name;
        $sql .=" WHERE " . static::$db_fields[0] . "=" .$database->escape_value($this->vartotojas_id);
        $sql .=" LIMIT 1";
        $database->query($sql);
        return ($database->affected_rows()==1) ? true : false;
    }


}

?>