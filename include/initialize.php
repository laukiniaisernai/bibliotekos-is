<?php
 // CIA LOADINSIM DAUGUMA FAILŲ, KAD NEREIKĖTŲ DARYT VISUR INCLUDE KLASIŲ IR PNŠ
error_reporting( error_reporting() & ~E_NOTICE & ~E_DEPRECATED );
defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

defined('SITE_ROOT') ? null :
    define('SITE_ROOT', DS.'wamp'.DS.'www'.DS.'bibliotekos-is');

defined('LIB_PATH') ? null : define('LIB_PATH', SITE_ROOT.DS.'include');

//includeinu configs faila, del DB prisijungimo duomenu(gal reikes lentelei sukurt atskira naudotoja?)
require_once(LIB_PATH.DS."configs.php");
//prijungiam funkcijas prie failo
require_once(LIB_PATH.DS."functions.php");

//pagrindiniai failai
require_once(LIB_PATH.DS."database.php");
require_once(LIB_PATH.DS."database_object.php");
require_once(LIB_PATH.DS."session.php");


//cia su DB susijusios klases, failai
require_once(LIB_PATH.DS."user.php");
require_once(LIB_PATH.DS."book.php");
require_once(LIB_PATH . DS . "comment.php");

?>