<?php
/**
 * Created by PhpStorm.
 * User: Mantas
 * Date: 12/4/2015
 * Time: 12:24
 */
    require_once(LIB_PATH.DS."database.php");

    class Book extends DatabaseObject {
        protected static $table_name = "knygos";
        protected static $db_fields = array('knyga_id', 'pavadinimas', 'isbn', 'pusl_skaicius','leidimo_metai','aprasymas','virselio_nuoroda');

        public $knyga_id;
        public $pavadinimas;
        public $isbn;
        public $pusl_skaicius;
        public $leidimo_metai;
        public $aprasymas;
        public $virselio_nuoroda;

        public static function InsertBook($pavadinimas, $isbn, $pusl_skaicius , $leidimo_metai, $aprasymas, $virselio_nuoroda)
        {
            $sql= "INSERT INTO ".static::$table_name."(pavadinimas, isbn, pusl_skaicius, leidimo_metai, aprasymas, virselio_nuoroda)
            VALUES ('$pavadinimas', '$isbn', '$pusl_skaicius' , '$leidimo_metai', '$aprasymas', '$virselio_nuoroda')";
            Book::executeSql($sql, 0);
        }
        public static function Bookid($isbn)
        {
            $sql = "SELECT knyga_id FROM ".static::$table_name." WHERE isbn = '$isbn' ;";
            $knyga_id = Book::executeSql($sql,1);
            return $knyga_id;

        }
        public static function Authorid($vardas, $pavarde)
        {
            $sql = "SELECT autorius_id FROM autoriai WHERE (vardas = '$vardas') AND (pavarde = '$pavarde') ;";
            return $knyga_id = Book::executeSql($sql,1);
        }
        public static function Genreid($zanras)
        {
            $sql = "SELECT kategorijos_id FROM kategorija WHERE (kategorijos_pavadinimas = '$zanras') ;";
            $zanro_id = Book::executeSql($sql,1);
        }
        public static function InsertAuthorBookConnection($book_id, $author_id){
            $sql= "INSERT INTO knygos_autoriai_jungtis (knyga_id, autorius_id) VALUES ('$book_id','$author_id' )";
            Book::executeSql($sql, 0);
        }
        public static function InsertGenreBookConnection($book_id, $genre_id)
        {
            $sql = "INSERT INTO knygos_kategorijos_jungtis (knyga_id, knygos_kategorijos_id) VALUES ('$book_id','$genre_id' )";
            Book::executeSql($sql, 0);
        }
        public function comments() {
            return Comment::find_comments_on($this->knyga_id);
        }

    }
?>