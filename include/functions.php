<?php

//VISOKIOS BASIC FUNKCIJOS ( GALIMA PILDYT, PAGAL POREIKIUS )

function strip_zeros_from_date($string = "") {
    //remove 0
    $no_zeros = str_replace('*0','', $string);
    //remove * if no 0 found
    $clean_str = str_replace('*','', $no_zeros);
    return $clean_str;
}

function redirect_to($location = NULL) {
    if($location != NULL){
        header("Location: {$location}");
    }
}

function output_message($message = "") {
    if(!empty($message)) {
        return "<p class=\"message\"> {$message} </p>";
    } else {
        return "";
    }
}

function __autoload($class_name) {
    $class_name = strtolower($class_name);
    $path = LIB_PATH.DS."{$class_name}.php";
    if(file_exists($path)) {
        require_once($path);
    } else {
        die ("The file {$class_name}.php could not be found.");
    }
}

function include_layout_template($template="") {
    include(SITE_ROOT.DS.'public'.DS.'layouts'.DS.$template);
}

function log_action($action, $message="") {
    if(file_exists(SITE_ROOT.DS.'logs'.DS.'action_log.txt')) {
        ;
    } else {
        touch(SITE_ROOT.DS.'logs'.DS.'action_log.txt');
    }
    if($handle=fopen(SITE_ROOT.DS.'logs'.DS.'action_log.txt','a')) {
        $current_time=strftime('%Y-%m-%d %H:%M:%S',time());
        $log_string=$current_time . " | {$action}: {$message} \r\n";
        fwrite($handle,$log_string);
        fclose($handle);
    } else {
        echo "File not opened for writing.";
    }
}

function datetime_to_text($datetime="") {
    $unixtime = strtotime($datetime);
    return strftime("%B %d, %Y at %H:%M:%S", $unixtime);
}

function theme_header($pageShortName) {
    echo '<div id="header">
    <div id="logo">
        <a href="index.php"><img src="images/logo.png" alt="LOGO"></a>
    </div>
    <div id="navigation">
        <ul id="primary">
            <li ',($pageShortName == 'home' ? 'class=selected' : ''), '>
                <a href="index.php">Home</a>
            </li>
            <li ',($pageShortName == 'new' ? 'class=selected' : ''), '>
                <a href="new.php">New Books</a>
            </li>
            <li ',($pageShortName == 'faq' ? 'class=selected' : ''), '>
                <a  href="faq.php">Faq</a>
            </li>
            <li ',($pageShortName == 'giveaway' ? 'class=selected' : ''), '>
                <a href="sale.php">Giveaway</a>
            </li>
        </ul>
        <ul id="secondary">
            <li ',($pageShortName == 'cart' ? 'class=selected' : ''), '>
                <a  href="checkout.php">Cart</a>
            </li>';

}

function return_city_number ($enteretdCity) {

    $cityArray = [
        "Vilnius" => 1,
        "Kaunas" => 2,
        "Panevezys" => 3,
        "Plunge" => 4,
        "Vilkaviskis" => 5,
        "Marijampole" => 6,
        "Mazeikiai" => 7,
        "Siauliai" => 8,
        "Klaipeda" => 9,
        "Alytus" => 10,
        "Druskininkai" => 11,
        "Ignalina" => 12,
        "Ukmerge" => 13,
        "Kedainiai" => 14
    ];

    foreach($cityArray as $key=>$value) {
        if($key == $enteretdCity) {
            return $value;
        }
    }
}

?>