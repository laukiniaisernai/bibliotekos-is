<?php
require_once('../include/initialize.php');

if(!$session->is_logged_in()) {
    redirect_to("index.php");
}

$user_id = $session->return_session_id();
$user_id = intval($user_id);
$user_found = User::find_user_by_id($user_id);
$user_found = get_object_vars($user_found);

if(!isset($user_found)) {
    $session->message("User could not be found.");
    redirect_to("index.php");
}

?>

<!DOCTYPE html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
    <meta charset="UTF-8">
    <title>Profile | Wild Boars Library</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<div id="background">
    <div id="page">
        <?php
        theme_header("home");
        if(!$session->is_logged_in()) {
            echo '<li>
                <a  href="login.php">Login</a> | <a href="signup.php">Signup</a>
               </li>';
        } else {
            echo '<li>
                    <a href="logout.php">Logout </a> | <a href="profile.php">Profile</a>
                </li>';
        }
        include_layout_template('header.php');
        ?>
        <div id="contents">
            <div id="login-box">
                <ul>
                            <li>
                                <p>
                                    Full name: <?php echo $user_found['vardas'] ." ". $user_found['pavarde'] . "</br>"; ?>
                                </p>
                            </li>
                            <li>
                                <p>
                                    El pastas: <?php echo $user_found['el_pastas'] ?>
                                </p>
                            </li>
                            <li>
                                <p>
                                    Cellphone number: <?php echo $user_found['tel_nr'] ?>
                                </p>
                            </li>
                            <li>
                                <p>
                                    City of registration:
                                    <?php
                                    $city = City::find_city( $user_found['miestas_id']);
                                    foreach($city as $raktas=>$reiksme) {
                                        echo $reiksme->miesto_pavadinimas;
                                    }
                                    ?>
                                </p>
                            </li>
                            <li>
                                <p>
                                    Skola bibliotekai: <?php echo $user_found['delspinigiai']; ?> &euro;
                                </p>
                            </li>
                            <li>
                                <p>
                                    Nusibaudimai sistemoje: <?php if($user_found['ar_uzblokuotas'] == "ne"){
                                        echo "Nusibaudimų sistemoje neturite.";
                                    } else {
                                        echo "Kurwa gaidy pasmaugsiu.";
                                    } ?>
                                </p>
                            </li>
                </ul>
            </div>
        </div>
        <?php include_layout_template('footer.php'); ?>
    </div>
</div>
</body>
</html>

<?php if(isset($database)){$database->close_connection();} ?>
