<?php
    require_once('../include/initialize.php');

if($session->is_logged_in()) {
    redirect_to("index.php");
}

if(isset($_POST['submit'])){

    $email = trim($_POST['email']);
    $password = trim($_POST['password']);

    $found_user = User::authenticate($email, $password);
    if($found_user){
        $testUser = get_object_vars($found_user);
        if($testUser['ar_uzblokuotas'] == "ne"){
            $session->login($found_user);
            log_action('Login',"{$found_user->email} user logged in.");
            redirect_to("index.php");
        } else {
            $message = "We are sorry to say, but you have been banned from our system.";
        }
    } else {
        $message = "E-mail adress/password combination incorrect.";
    }
} else {
    $email="";
    $password="";
}

?>

<!DOCTYPE html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
    <meta charset="UTF-8">
    <title>Log In| Wild Boars Library</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<div id="background">
    <div id="page">
        <?php
        theme_header("login");
        if(!$session->is_logged_in()) {
            echo '<li>
                <a  href="login.php">Login</a> | <a href="signup.php">Signup</a>
               </li>';
        } else {
            echo '<li>
                    <a href="logout.php">Logout</a> | <a href="profile.php">Profile</a>
                </li>';
        }
        include_layout_template('header.php');
        ?>
        <div id="contents">
            <div id="login-box">
                <form method="POST">
                    <?php echo "<p> $message </p>" ?>
                    <ul>
                        <li>
                            <p>E-mail adress:</p>
                            <input type="email" name="email" size="25">
                        </li>
                        <li>
                            <p>Password:</p>
                            <input type="password" name="password" size="25">
                        </li>
                    </ul>
                        <ul>
                            <li>
                                <input class="button-orig" type="submit" name="submit" value="Login">
                            </li>
                        </ul>
                </form>
            </div>
        </div>
        <?php include_layout_template('footer.php'); ?>
    </div>
</div>
</body>
</html>

<?php if(isset($database)){$database->close_connection();} ?>
