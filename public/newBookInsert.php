<?php   require_once('../include/initialize.php'); ?>
<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <title>Knygos pridėjimo foma</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<div id="background">
    <div id="page">
        <?php
        // theme_header("home")
        if(isset($_POST['submit'])){

            $pavadinimas = trim($_POST['pavadinimas']);
            $isbn  = trim($_POST['isbn']);
            $pusl_skaicius = trim($_POST['pusl_skaicius']);
            $leidimo_metai  = trim($_POST['leidimo_metai']);
            $aprasymas  = $_POST ['aprasymas'];
            $virselio_nuoroda  = $_POST ['virselio_nuoroda'];
            $knyga_id= Book::Bookid($isbn);

            Book::InsertBook($pavadinimas,$isbn, $pusl_skaicius , $leidimo_metai, $aprasymas, $virselio_nuoroda);

            $autorius_id = $_POST['vardas'];
            $knyga_id = Book::Bookid($isbn);
            if($autorius_id > 0) {
                Book::InsertAuthorBookConnection($knyga_id,$autorius_id );
            }

            $zanro_id = $_POST['zanras'];
            if ($zanro_id > 0)
            {
                Book::InsertGenreBookConnection($knyga_id,$zanro_id );
            }

        }
        ?>

        <div id="contents">
            <div id="">
                <form method="POST">
                    <ul>
                        <li>
                            <p>Pavadinimas:</p>
                            <input type="pavadinimas" name="pavadinimas" size="50">
                        </li>
                        <li>
                            <p>ISBN:</p>
                            <input type="isbn" name="isbn" size="13">
                        </li>
                        <li>
                            <p>Leidimo metai:</p>
                            <input type="leidimo_metai" name="leidimo_metai" size="4">
                        </li>
                        <li>
                            <p>Autorius:</p>
                            <select name = "vardas">
                                <option >Pasirinkite autorių</option>
                                <?php
                                $sql = mysql_query("SELECT autorius_id, vardas, pavarde FROM autoriai");
                                while ($row = mysql_fetch_array($sql)){
                                    echo "<option value=". $row['autorius_id'] .">" . $row['vardas']. ' '. $row['pavarde'] . "</option>";
                                }
                                ?>
                            </select>
                        </li>
                        <li>
                            <p>Žanras:</p>
                            <select name = "zanras">
                                <option >Pasirinkite žanrą</option>
                                <?php
                                $sql = mysql_query("SELECT kategorijos_id, kategorijos_pavadinimas FROM kategorija");

                                while ($row = mysql_fetch_array($sql)){
                                    echo "<option value=". $row['kategorijos_id'] .">" . $row['kategorijos_pavadinimas'] . "</option>";
                                }
                                ?>
                            </select>
                        </li>
                        <li>
                            <p class="redText">
                                <?php echo $message; ?>
                            </p>
                        </li>

                        <li>
                            <p>Puslapių skaičius:</p>
                            <input type="pages" name="pusl_skaicius" size="4">
                        </li>
                        <li>
                            <p>Aprašymas :</p>
                            <textarea name="aprasymas" cols="40" rows="10" ></textarea>
                        </li>
                        <li>
                            <p>Viršelio nuoroda :</p>
                            <textarea name="virselio_nuoroda" cols="40" rows="5" ></textarea>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <input class="button-orig" type="submit" name="submit" value="Pridėti">
                        </li>
                    </ul>
                </form>
            </div>
        </div>

    </div>
</div>
</body>
</html>
<?php if(isset($database)){$database->close_connection();} ?>

