<?php
require_once('../include/initialize.php');

if(isset($_POST['submit'])) {

    $message = "";

    $user = new User();
    $user->vartotojas_id = NULL;
    $user->vardas = $_POST['firstName'];
    $user->pavarde = $_POST['lastName'];
    $user->el_pastas = $_POST['eMail'];
    $user->slaptazodis = $_POST['password'];
    $user->tel_nr = $_POST['telNum'];
    $user->busenos_id=1;
    $user->ar_uzblokuotas='ne';
    $selectedCity = $_POST['citySelect'];
    if($selectedCity != "Select your city..") {
        $user->miestas_id = return_city_number ($selectedCity);
        $user->miestas_id;
    } else {
        $message = "You chose your city wrong.";
    }
    $user->delspinigiai = 0;

    $checkEmail = $_POST['eMailConfirm'];
    $checkPassword = $_POST['passwordConfirm'];
    if($user->slaptazodis != $checkPassword && $user->el_pastas != $checkEmail) {
        $message = "Your passwords and e-mail addresses do not match.";
    }else if($user->el_pastas != $checkEmail) {
        $message = "Your emails do not match.";
    }else if($user->slaptazodis != $checkPassword) {
        $message = "Your passwords do not match.";
    }

    if(empty($message)) {
        $user->save();
        $message = "You have been registered.";
    }
}

?>

<!DOCTYPE html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
    <meta charset="UTF-8">
    <title>Sign up | Wild Boars Library</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<div id="background">
    <div id="page">
        <?php
        theme_header("signup");
        if(!$session->is_logged_in()) {
            echo '<li>
                <a  href="login.php">Login</a> | <a href="signup.php">Signup</a>
               </li>';
        } else {
            echo '<li>
                    <a href="logout.php">Logout</a>
                </li>';
        }
        include_layout_template('header.php');
        ?>
        <div id="contents">
            <form action="signup.php" method="POST">
                <ul>
                    <li>
                        <p>Your name:<span title="You must enter this information.">*</span></p>
                        <input name="firstName" type="text" size="25">
                    </li>
                    <li>
                        <p>Your last name:<span title="You must enter this information.">*</span></p>
                        <input name="lastName" type="text" size="25">
                    </li>
                    <li>
                        <p>E-mail adress:<span title="You must enter this information.">*</span></p>
                        <input name="eMail" type="email" size="25">
                    </li>
                    <li>
                        <p>E-mail adress (Confirm):<span title="You must enter this information.">*</span></p>
                        <input name="eMailConfirm" type="email" size="25">
                    </li>
                    <li>
                        <p>Password:<span title="You must enter this information.">*</span></p>
                        <input name="password" type="password" size="25">
                    </li>
                    <li>
                        <p>Password (Confirm):<span title="You must enter this information.">*</span></p>
                        <input name="passwordConfirm" type="password" size="25">
                    </li>
                    <li>
                        <p>Telephone number:</p>
                        <input name="telNum" type="number" size="25">
                    </li>
                    <li>
                        <p>You city:<span title="You must enter this information.">*</span></p>
                        <select name="citySelect">
                            <option>Select your city..</option>
                            <option value="Vilnius">Vilnius</option>
                            <option value="Kaunas">Kaunas</option>
                            <option value="Panevezys">Panevezys</option>
                            <option value="Plunge">Plunge</option>
                            <option value="Vilkaviskis">Vilkaviskis</option>
                            <option value="Marijampole">Marijampole</option>
                            <option value="Mazeikiai">Mazeikiai</option>
                            <option value="Siauliai">Siauliai</option>
                            <option value="Klaipeda">Klaipeda</option>
                            <option value="Alytus">Alytus</option>
                            <option value="Druskininkai">Druskininkai</option>
                            <option value="Ignalina">Ignalina</option>
                            <option value="Kedainiai">Kedainiai</option>
                            <option value="Ukmerge">Ukmerge</option>
                        </select>
                    </li>
                    <li>
                        <p class="redText">
                            <?php echo $message; ?>
                        </p>
                    </li>
                </ul>
                <ul>
                    <li>
                        <input class="button-orig" type="submit" value="Sign up" name="submit">
                    </li>
                </ul>
            </form>
        </div>
        <?php include_layout_template('footer.php'); ?>
    </div>
</div>
</body>
</html>
