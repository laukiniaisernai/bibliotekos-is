<?php
    require_once("../include/initialize.php");

//paziet kuriam puslapį vartotojas dabar yra, per URl (page?=1, page?=2 ir pns)
$page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
//kiek knygu rodyt puslapi
$per_page = 8;
//suskaiciuot kiek isviso knygu yra
$total_count = Book::count_all();

//naudojam konstruktoriu ir nurodom pagination duomenis
$pagination = new Pagination($page, $per_page, $total_count);

//offset tai nurodo nuo KURIO iraso pradet skaityt is db lenteles, tarkim offset 5, tada prades nuo 5 iraso, tai yra ims 6 ir toliau, per_page = 5, vadinasi ims kitus 5 irasus, t.y 6-10.
$sql  = "SELECT * FROM knygos ";
$sql .= "LIMIT {$per_page} ";
$sql .= "OFFSET {$pagination->offset()}";

$book = Book::find_by_sql($sql);

?>
<!DOCTYPE html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <title>Home | Wild Boars Library</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<div id="background">
    <div id="page">
        <?php
        theme_header("home");
        if(!$session->is_logged_in()) {
            echo '<li>
                <a  href="login.php">Login</a> | <a href="signup.php">Signup</a>
               </li>';
        } else {
            echo '<li>
                    <a href="logout.php">Logout</a> | <a href="profile.php">Profile</a>
                </li>';
        }
        include_layout_template('header.php');
        ?>
        <div id="contents">
            <div id="adbox">
                <div id="search">
                    <h3>Quick Search</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consec tetur adipiscing elit, Praesent diam odio, commodo vittae sagittis vel,
                    </p>
                    <form action="index.php" method="post">
                        <ul>
                            <li>
                                <select id="size">
                                    <option>Search by Size</option>
                                </select>
                            </li>
                            <li>
                                <select id="color">
                                    <option>Search by Color</option>
                                </select>
                            </li>
                            <li>
                                <select id="brand">
                                    <option>Search by Brand</option>
                                </select>
                            </li>
                            <li>
                                <select id="style">
                                    <option>Search by Style</option>
                                </select>
                            </li>
                        </ul>
                        <input type="submit" value="Find My Pair!" class="button">
                    </form>
                </div>
                <img src="images/library3.png" height="355" width="618" alt="Promo"></span>
            </div>
            <div id="main">
                <div id="featured">
                    <h4><span>Turimos knygos</span></h4>
                    <ul class="items">
                        <?php foreach($book as $key=>$value): ?>
                        <li class="book_title">
                            <a href="book_display.php?id=<?php echo $value->knyga_id; ?>"> <img height="169" width="150" src="<?php echo $value->virselio_nuoroda; ?>" alt="Img"> <?php echo $value->pavadinimas; ?> </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div id="pagination">
                    <?php
                    if($pagination->total_pages() > 1) {
                        if($pagination->has_next_page()) {
                            echo "<a href=\"index.php?page=";
                            echo $pagination->next_page();
                            echo "\">Next &raquo; </a>";
                        }
                        for($i=1 ; $i <= $pagination->total_pages(); $i++) {
                            if($i == $page){
                                echo "<span class=\"selection\">{$i}</span>";
                            }else {
                                echo "<a href=\"index.php?page={$i}\">{$i}</a>";
                            }
                        }
                        if($pagination->has_previous_page()) {
                            echo " <a href=\"index.php?page=";
                            echo $pagination->previous_page();
                            echo "\">&laquo; Previous</a>";
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php include_layout_template('footer.php'); ?>
    </div>
</div>
</body>
</html>
