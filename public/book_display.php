<?php
require_once('../include/initialize.php');
$book = Book::find_by_id($_GET['id']);
if(!$book) {
    if($session->is_logged_in()) {
        $session->message('The book could not be found.');
        redirect_to('index.php');
    }
}

if(!$book) {
    $session->message("The photograph could not be found.");
    redirect_to('index.php');
}

if(isset($_POST['submit'])) {
    $komentaras = trim($_POST['textbox']);
    if(strlen($komentaras) > 355) {
        $message = "Your comment is too long";
        redirect_to("book_display.php?id={$book->knyga_id}");
    } else {
        $new_comment = Comment::make($book->knyga_id,intval($session->return_session_id()), $komentaras);
        if($new_comment && $new_comment->save()) {
            redirect_to("book_display.php?id={$book->knyga_id}");
        } else {
            $message = "The comment could not be saved.";
        }
    }

}

$comment = $book->comments();


?>

<!DOCTYPE html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title> <?php echo $book->pavadinimas; ?> | Wild Boars Library</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<div id="background">
    <div id="page">
        <?php
        theme_header("home");
        if(!$session->is_logged_in()) {
            echo '<li>
                <a  href="login.php">Login</a> | <a href="signup.php">Signup</a>
               </li>';
        } else {
            echo '<li>
                    <a href="logout.php">Logout</a> | <a href="profile.php">Profile</a>
                </li>';
        }
        include_layout_template('header.php');
        ?>
        <div id="contents">
            <div id="book_holder">
                <h1>
                    <?php echo $book->pavadinimas; ?>
                </h1>
                <div id="img_holder">
                    <img src="<?php echo $book->virselio_nuoroda; ?>">
                </div>
                <div id="short_description">
                    <ul>
                        <li>
                           Išleidimo metai: <?php echo $book->leidimo_metai; ?>
                        </li>
                        <li>
                            ISBN: <?php echo $book->isbn; ?>
                        </li>
                        <li>
                            Puslapių skaičius: <?php echo $book->pusl_skaicius; ?>
                        </li>
                    </ul>
                </div>
                <div id="long_description">
                    <h2>Aprašymas</h2>
                    <p>
                        <?php echo $book->aprasymas;  ?>
                    </p>
                </div>
            </div>
            <div id="comment_holder">
                <?php if($session->is_logged_in()) { ?>
                <h3> Komentarai </h3>
                <?php echo output_message($message); ?>
                <div>
                    <form action="book_display.php?id=<?php echo $book->knyga_id;//reikia action nurodyt atgal i ta pati puslapi, kad matytum komentara ( PHP turi kviesti duom baze ) ?>" method="POST">
                        <ul>
                            <li>
                                <textarea maxlength="355" value="" name="textbox" ></textarea>
                            </li>
                            <li>
                                <input id="button-coments" type="submit" name="submit" value="Komentuoti">
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
            <?php } else {
                echo "<p><i>You need to log in to be able to post comments. Sorry about that..</i></p>";
            } ?>
            <div id="comment_viewer">
                <ul>
                    <?php foreach($comment as $key=>$value): ?>
                        <div class="comment-author">
                            <?php
                                $user = User::find_by_sql("SELECT * FROM vartotojai WHERE vartotojas_id='{$value->vartotojas_id}' LIMIT 1");
                                foreach($user as $key2=>$value2) {
                                    echo $value2->el_pastas;
                                }
                            ?>
                        </div>
                        <div class="comment-body">
                            <?php echo $value->komentaras; ?>
                        </div>
                        <div class="comment-date">
                            <?php echo datetime_to_text($value->parasymo_data); ?>
                        </div>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <?php include_layout_template('footer.php'); ?>
    </div>
</div>
</body>
</html>

<?php if(isset($database)){$database->close_connection();} ?>