<?php
require_once('../../include/initialize.php');

?>

<!DOCTYPE html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
    <meta charset="UTF-8">
    <title>Admin zone | Wild Boars Library</title>
    <link rel="stylesheet" href="../css/style.css" type="text/css">
</head>
<body>
<div id="background">
    <div id="page">
        <?php
            include_layout_template('admin_header.php');
        ?>
        <div id="contents">
            <ul>
                <li>
                    <a href="user_display.php">
                        Browse users
                    </a>
                </li>
                <li>
                    <a href="newBookInsert.php">
                        Knygos pridėjimas <!-- CIA AISTES FAILUI -->
                    </a>
                </li>
            </ul>
        </div>
        <?php include_layout_template('admin_footer.php'); ?>
    </div>
</div>
</body>
</html>

<?php if(isset($database)){$database->close_connection();} ?>
