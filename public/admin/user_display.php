<?php
require_once('../../include/initialize.php');
    $user = User::find_all();
    $comments = Comment::find_all();

    //get the current page user is in
    $page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
    //set max pictures per page
    $per_page = 10;
    //how many records are there in total
    $total_count = User::count_all();

    //$photo = Photograph::find_all();

    $pagination = new Pagination($page, $per_page, $total_count);

    $sql  = "SELECT * FROM vartotojai ";
    $sql .= "LIMIT {$per_page} ";
    $sql .= "OFFSET {$pagination->offset()}";

    $user = User::find_by_sql($sql);

?>

<!DOCTYPE html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
    <meta charset="UTF-8">
    <title>All users | Wild Boars Library</title>
    <link rel="stylesheet" href="../css/style.css" type="text/css">
</head>
<body>
<div id="background">
    <div id="page">
        <?php
        include_layout_template('admin_header.php');
        ?>
        <div id="contents">
            <table>
                <thead>
                    <tr>
                        <th>
                            Pilnas vardas
                        </th>
                        <th>
                            El. paštas
                        </th>
                        <th>
                            Telefono numeris:
                        </th>
                        <th>
                            Delspinigiai
                        </th>
                        <th>
                            Ar užblokuotas
                        </th>
                        <th>

                        </th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($user as $key => $value): ?>
                    <tr>
                        <td>
                            <?php echo $value->vardas . " " . $value->pavarde; ?>
                        </td>
                        <td>
                            <?php echo $value->el_pastas; ?>
                        </td>
                        <td>
                            <?php echo $value->tel_nr; ?>
                        </td>
                        <td>
                            <?php echo $value->delspinigiai; ?>
                        </td>
                        <td>
                            <?php echo $value->ar_uzblokuotas; ?>
                        </td>
                        <td>
                            <a href="ban_user.php?id=<?php echo $value->vartotojas_id; ?>">
                                BAN USER
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <?php include_layout_template('admin_footer.php'); ?>
    </div>
</div>
</body>
</html>

<?php if(isset($database)){$database->close_connection();} ?>
