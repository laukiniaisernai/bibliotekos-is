<?php
require_once("../include/initialize.php");
?>
<!DOCTYPE html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>Checkout - RubberNLaces Website Template</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div id="background">
		<div id="page">
			<?php
			theme_header("cart");
			if(!$session->is_logged_in()) {
				echo '<li>
                <a  href="login.php">Login</a> | <a href="signup.php">Signup</a>
               </li>';
			} else {
				echo '<li>
                    <a href="logout.php">Logout</a>
                </li>';
			}
			include_layout_template('header.php');
			?>
			<div id="contents">
				<div id="checkout">
					<h4><span>Checkout</span></h4>
					<table>
						<thead>
							<tr>
								<th>Order no. 0017893547</th>
								<th>Quantity</th>
								<th>Size</th>
								<th>Price</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><img src="images/leather-boots3.jpg" alt="Thumbnail"> <b>Hukbalahap Boots</b>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis cursus gravida. Suspendisse ut ligula tristique velit blandit blandit vel sit amet ipsum. Nam accumsan turpid id mauris fermentum
									</p></td>
								<td><a href="checkout.php" class="minus"></a> <a href="checkout.php" class="plus"></a>
									<input type="text" value="1" class="txtfield"></td>
								<td><a href="checkout.php" class="minus"></a> <a href="checkout.php" class="plus"></a>
									<input type="text" value="7.5" class="txtfield"></td>
								<td class="last"><div>
										$8.50 <a href="checkout.php">Delete</a>
									</div></td>
							</tr> <tr>
								<td><img src="images/leather-boots3.jpg" alt="Thumbnail"> <b>Hukbalahap Boots</b>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis cursus gravida. Suspendisse ut ligula tristique velit blandit blandit vel sit amet ipsum. Nam accumsan turpid id mauris fermentum
									</p></td>
								<td><a href="checkout.php" class="minus"></a> <a href="checkout.php" class="plus"></a>
									<input type="text" value="1" class="txtfield"></td>
								<td><a href="checkout.php" class="minus"></a> <a href="checkout.php" class="plus"></a>
									<input type="text" value="7.5" class="txtfield"></td>
								<td class="last"><div>
										$8.50 <a href="checkout.php">Delete</a>
									</div></td>
							</tr> <tr>
								<td><img src="images/leather-boots3.jpg" alt="Thumbnail"> <b>Hukbalahap Boots</b>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis cursus gravida. Suspendisse ut ligula tristique velit blandit blandit vel sit amet ipsum. Nam accumsan turpid id mauris fermentum
									</p></td>
								<td><a href="checkout.php" class="minus"></a> <a href="checkout.php" class="plus"></a>
									<input type="text" value="1" class="txtfield"></td>
								<td><a href="checkout.php" class="minus"></a> <a href="checkout.php" class="plus"></a>
									<input type="text" value="7.5" class="txtfield"></td>
								<td class="last"><div>
										$8.50 <a href="checkout.php">Delete</a>
									</div></td>
							</tr> <tr>
								<td><img src="images/leather-boots3.jpg" alt="Thumbnail"> <b>Hukbalahap Boots</b>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis cursus gravida. Suspendisse ut ligula tristique velit blandit blandit vel sit amet ipsum. Nam accumsan turpid id mauris fermentum
									</p></td>
								<td><a href="checkout.php" class="minus"></a> <a href="checkout.php" class="plus"></a>
									<input type="text" value="1" class="txtfield"></td>
								<td><a href="checkout.php" class="minus"></a> <a href="checkout.php" class="plus"></a>
									<input type="text" value="7.5" class="txtfield"></td>
								<td class="last"><div>
										$8.50 <a href="checkout.php">Delete</a>
									</div></td>
							</tr>
						</tbody>
					</table>
					<a href="checkout.php" class="proceed-btn">Proceed to Checkout</a>
				</div>
			</div>
			<?php include_layout_template('footer.php'); ?>
		</div>
	</div>
</body>
</html>