<?php
	require_once("../include/initialize.php");
?>
<!DOCTYPE html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>New Arrivals - RubberNLaces Website Template</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div id="background">
		<div id="page">
			<?php
			theme_header("new");
			if(!$session->is_logged_in()) {
				echo '<li>
                <a  href="login.php">Login</a> | <a href="signup.php">Signup</a>
               </li>';
			} else {
				echo '<li>
                    <a href="logout.php">Logout</a>
                </li>';
			}
			include_layout_template('header.php');
			?>
			<div id="contents">
				<h4><span>New Arrivals</span></h4>
				<div id="stocks">
					<ul>
						<li>
							<a href="product.php" class="preview" title="Preview"> <img src="images/boots-brown.jpg" alt="Img"> <span class="icon"></span></a> <span>$8.50</span> Hukbalahap Boots <em>Quick Shop</em> <a href="checkout.php" class="btn-cart">Add to Cart</a> <a href="checkout.php" class="btn-wish">Add to Wishlist</a>
						</li>
						<li>
							<a href="product.php" class="preview" title="Preview"> <img src="images/boots-violet.jpg" alt="Img"> <span class="icon"></span></a> <span>$8.50</span> Hukbalahap Boots <em>Quick Shop</em> <a href="checkout.php" class="btn-cart">Add to Cart</a> <a href="checkout.php" class="btn-wish">Add to Wishlist</a>
						</li>
						<li>
							<a href="product.php" class="preview" title="Preview"> <img src="images/boots-yellow.jpg" alt="Img"> <span class="icon"></span></a> <span>$8.50</span> Hukbalahap Boots <em>Quick Shop</em> <a href="checkout.php" class="btn-cart">Add to Cart</a> <a href="checkout.php" class="btn-wish">Add to Wishlist</a>
						</li>
						<li>
							<a href="product.php" class="preview" title="Preview"> <img src="images/boots-green.jpg" alt="Img"> <span class="icon"></span></a> <span>$8.50</span> Hukbalahap Boots <em>Quick Shop</em> <a href="checkout.php" class="btn-cart">Add to Cart</a> <a href="checkout.php" class="btn-wish">Add to Wishlist</a>
						</li>
						<li>
							<a href="product.php" class="preview" title="Preview"> <img src="images/boots-brown.jpg" alt="Img"> <span class="icon"></span></a> <span>$8.50</span> Hukbalahap Boots <em>Quick Shop</em> <a href="checkout.php" class="btn-cart">Add to Cart</a> <a href="checkout.php" class="btn-wish">Add to Wishlist</a>
						</li>
						<li>
							<a href="product.php" class="preview" title="Preview"> <img src="images/boots-violet.jpg" alt="Img"> <span class="icon"></span></a> <span>$8.50</span> Hukbalahap Boots <em>Quick Shop</em> <a href="checkout.php" class="btn-cart">Add to Cart</a> <a href="checkout.php" class="btn-wish">Add to Wishlist</a>
						</li>
						<li>
							<a href="product.php" class="preview" title="Preview"> <img src="images/boots-yellow.jpg" alt="Img"> <span class="icon"></span></a> <span>$8.50</span> Hukbalahap Boots <em>Quick Shop</em> <a href="checkout.php" class="btn-cart">Add to Cart</a> <a href="checkout.php" class="btn-wish">Add to Wishlist</a>
						</li>
						<li>
							<a href="product.php" class="preview" title="Preview"> <img src="images/boots-green.jpg" alt="Img"> <span class="icon"></span></a> <span>$8.50</span> Hukbalahap Boots <em>Quick Shop</em> <a href="checkout.php" class="btn-cart">Add to Cart</a> <a href="checkout.php" class="btn-wish">Add to Wishlist</a>
						</li>
					</ul>
				</div>
				<div class="footer">
					<h4><span>Similar Items</span></h4>
					<ul class="items">
						<li>
							<a href="product.php"> <img src="images/boots-brown.jpg" alt="Img"> <span>$8.50</span> Hukbalahap Boots</a>
						</li>
						<li>
							<a href="product.php"> <img src="images/boots-violet.jpg" alt="Img"> <span>$8.50</span> Hukbalahap Boots</a>
						</li>
						<li>
							<a href="product.php"> <img src="images/boots-yellow.jpg" alt="Img"> <span>$8.50</span> Hukbalahap Boots</a>
						</li>
						<li>
							<a href="product.php"> <img src="images/boots-green.jpg" alt="Img"> <span>$8.50</span> Hukbalahap Boots</a>
						</li>
					</ul>
				</div>
			</div>
			<?php include_layout_template('footer.php'); ?>
		</div>
	</div>
</body>
</html>